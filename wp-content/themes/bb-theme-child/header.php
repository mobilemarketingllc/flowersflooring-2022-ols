<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php do_action( 'fl_head_open' ); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php echo apply_filters( 'fl_theme_viewport', "<meta name='viewport' content='width=device-width, initial-scale=1.0' />\n" ); ?>
<?php echo apply_filters( 'fl_theme_xua_compatible', "<meta http-equiv='X-UA-Compatible' content='IE=edge' />\n" ); ?>
<noscript><meta http-equiv="refresh" content="5;URL=http://flowersflooring.com"></noscript>
<link rel="profile" href="https://gmpg.org/xfn/11" />
<?php

wp_head();

FLTheme::head();

?>
	<!-- Begin TVSquared Tracking Code -->
<script type="text/javascript">
 var _tvq = window._tvq = window._tvq || [];
 (function() {
 var u = (("https:" == document.location.protocol) ?
 "https://collector-16874.us.tvsquared.com/"
 :
 "http://collector-16874.us.tvsquared.com/");
 _tvq.push(['setSiteId', "TV-8136182754-1"]);
 _tvq.push(['setTrackerUrl', u + 'tv2track.php']);
 _tvq.push([function() {
 this.deleteCustomVariable(5, 'page')
 }]);
 _tvq.push(['trackPageView']);
 var d = document,
 g = d.createElement('script'),
 s = d.getElementsByTagName('script')[0];
 g.type = 'text/javascript';
 g.defer = true;
 g.async = true;
 g.src = u + 'tv2track.js';
 s.parentNode.insertBefore(g, s);
 })();
</script>
<!-- End TVSquared Tracking Code -->
</head>
<body <?php body_class(); ?><?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/WebPage"' ); ?>>
<?php

FLTheme::header_code();

do_action( 'fl_body_open' );

?>
<div class="fl-page">
	<?php

	do_action( 'fl_page_open' );

	FLTheme::fixed_header();

	do_action( 'fl_before_top_bar' );

	FLTheme::top_bar();

	do_action( 'fl_after_top_bar' );
	do_action( 'fl_before_header' );

	FLTheme::header_layout();

	do_action( 'fl_after_header' );
	do_action( 'fl_before_content' );

	?>
	<div id="fl-main-content" class="fl-page-content" itemprop="mainContentOfPage" role="main">
	<?php 
	if ( is_front_page() && is_home() ) {} elseif ( is_front_page() ) {} elseif ( is_home() ) {
	?>
	<h1 class="fl-heading display_none">
			<span class="fl-heading-text">Blog</span>
		</h1>
	<?php
	} else {}
	?>
		<?php do_action( 'fl_content_open' ); ?>
